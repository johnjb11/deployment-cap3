const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require('../auth')

// creating a product
router.post("/create", auth.verify,  (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin === true){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}else{
		res.send("Not Authorized");
	}

	
});


// retrieving all product
router.get('/all', (req, res) => {


	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

// retrieve all active product(s)
router.get('/', (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// retrieve a specific product

router.get('/:productId', (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.post('/search', (req,res)=> {
	productController.searchProduct(req.body).then(resultFromController => res.send(resultFromController))

})


// update product
router.put("/:productId/update", auth.verify, (req,res) => {
	
	if (auth.decode(req.headers.authorization))

	{	
		productController.updateProduct(req.params, req.body)
		.then(resultFromController=>res.send(resultFromController))
	} else {
		res.send("Authorized users only.")}
})

// archiving product

router.put('/:productId/archive', auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
} )


router.put('/:productId/activate', auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin === true){
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		return res.send('Not Authorized')
	}
} )



module.exports = router;

