const Product = require("../models/Product")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const User = require("../models/User")



//create product
module.exports.addProduct = (productDetails) => {
	const { name, description, type, price, stocks, availability, isOutofStock , isActive} = productDetails;
	
	const newProduct = new Product({
		name: name, 
		description: description,
		price: price,
		stocks: stocks,
		availability: availability,
		isOutofStock:isOutofStock,
		isActive:isActive
	})

	return newProduct.save().then((product, err) => {
		if(err) return false
		else return product
	})
}; 






//retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then(result =>{
		return result;
	})
}

module.exports.getAllActive = () => {
	return Product.find({}).then(result =>{
		return result;
	})
}

//retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		let product = {
			name: result.name,
			description: result.description,
			price:result.price,
			stocks:result.stocks,
			availability:result.availability
		}
		return product;
	})
}

module.exports.searchProduct = (reqBody) => {

	return Product.find({name: reqBody.name}).then(result => {
		return result
	}).catch((err)=> {
		return err
	})
}


//update product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks:reqBody.stocks,
		availability:reqBody.availability,
		isOutofStock:reqBody.isOutofStock,
		isActive:reqBody.isActive
	}
	
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then((product,error) => {
		if(error){
			return "Unsuccessful updating of product information."
		
		} else {
			return "Successfully updated product information"
		}
		})
}




//update product information / archiving
module.exports.archiveProduct = (reqParams) => {

	return Product.findByIdAndUpdate(reqParams.productId).then(result=> {
		if(result.isActive === true){

			result.isActive = false
			return result.save().then(()=> {
				return result
			}).catch(()=> {
				return false
			})
		} else {
			return result
		}
	})
}


module.exports.activateProduct = (reqParams) => {

	return Product.findByIdAndUpdate(reqParams.productId).then(result=> {
		if(result.isActive === false){

			result.isActive = true
			return result.save().then(()=> {
				return result
			}).catch(()=> {
				return false
			})
		} else {
			return result
		}
	})
}























