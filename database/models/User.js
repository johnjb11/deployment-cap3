const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName:
			{
				type: String,
				required: [true, "First name is required"]
	},

	lastName:
			{
				type: String,
				required: [true, "Last name is required"] 
	},
	address: {
				type: String,
				required: [true, "Address is required"] 
	},

	email: {
		type: String,
		required: [true, 'Enter your email address.']
	},

	password: {
		type: String,
		required: [true, 'Enter your password.']
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
		{
			productId: String,
			qty: Number,
			purchasedOn:{
				type: Date,
				default: new Date()
			},
			status:{
				type: String,
				default: "pending"
			}
		}
	]
})


module.exports = mongoose.model('User', userSchema)
