import {useState} from 'react';
import {Table} from 'react-bootstrap';


export default function newOrder ({orders}) {


	const {name, customer, stocks, price, firstName, lastName, availability} = orders
	
	
	function customerDetails (customer) {
		customer.preventDetails();
		fetch('https://deadpoolmagicshop.netlify.app/user', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			firstName(data.firstName)
			lastName(data.lastName)

		})
	}


	return (

		<Table>
		  <tbody>
		    <tr>
		      <td>
		      	<h2>{name}</h2>
		      	<span>Customer: </span>
		      	<span>{customer.id}</span>
		      	<span>{price} </span>
		      	<span>{stocks}</span>
		      	<span>{availability}</span>
		      	</td>
		      </tr>
		  </tbody>
		</Table>

	)
}
