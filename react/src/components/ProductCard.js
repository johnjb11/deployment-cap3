import {useState} from 'react'
import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export default function ProductCard({productProp}) {

console.log(productProp)


const {name, description, price, stocks, availability, _id} = productProp


    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

                <Card.Subtitle>Stocks:</Card.Subtitle>
                <Card.Text>{stocks}</Card.Text>

                <Card.Subtitle>Availability:</Card.Subtitle>
                <Card.Text>{availability}</Card.Text>

                <Card.Text>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Illo aliquid magnam repellendus quia ex perferendis cupiditate nemo, doloremque iure tempore perspiciatis hic nulla quas adipisci ipsum veritatis laboriosam natus nesciunt.</Card.Text>
                <Link className = "btn btn-primary" to = {`/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
