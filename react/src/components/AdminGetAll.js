import {useState, useEffect, useContext, Fragment} from 'react'
import {Row, Col, Form, FormControl, Container, Button} from 'react-bootstrap'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import AdminProdCard from '../components/AdminProductCard'
import AdminEdit from '../components/AdminEdit'

export default function AdminAllProducts(){

	const [adminProduct, setAdminProduct] = useState([])
	const [adminEditProduct, setAdminEditProduct] = useState([])
	const [name, setName] = useState('')

	function searchProduct(e) {
		e.preventDefault(e);

		fetch('https://deadpoolmagicshop.netlify.app/products/search',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name: name
			})
		})
		.then(res =>{
			console.log(res)
			return res.json()
		} )
		.then(data => {
			console.log(data)

			setAdminEditProduct(data.map(adminEditProduct => {
				return(
					<AdminEdit key={adminEditProduct.id} adminEditProp = {adminEditProduct}/>
				)
			}))
		}, [])
	}
	
	useEffect(() => {
		fetch('https://deadpoolmagicshop.netlify.app/products/all')
		.then(res => res.json())
		.then(data => {
		

			setAdminProduct(data.map((adminProduct, index) => {
				
				return(
					<Fragment>
					
					<AdminProdCard key={adminProduct.id} adminProdProp = {adminProduct}/>
								
					</Fragment>
				)
			}))
		})
	}, [])


	return(
		<Fragment>
		<Container>
		<div>
			
			</div>
			<Form>
				<Form.Group controlId="search-product">
					<Form.Label>Search for product name: </Form.Label>
					<Form.Control
						type= 'name'
						value= {name}
						onChange = {(e) => setName(e.target.value)} 
					/>
					</Form.Group>
    				<Button variant = 'primary' type= 'submit' id = 'submitBtn' onClick={(e)=> searchProduct(e)}>Search</Button>
			</Form>	

			<div>
			{adminEditProduct}
			</div>
	
			</Container>
		</Fragment>
	)
	
}
