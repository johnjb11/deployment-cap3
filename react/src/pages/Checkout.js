import {useState, useEffect, useContext, Fragment} from 'react';
import {Row, Col, Form, Button, CardGroup,Card, Table, Container} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import ViewCartCard from '../components/ViewCartCard'

export default function Checkout() {
	
	const {user, setUser} = useContext(UserContext)

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [address, setAddress] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	useEffect(() => {

		fetch(`https://deadpoolmagicshop.netlify.app/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		 	setName(data.name)
		 	setEmail(data.email)
		 	setAddress(data.address)
		 	setMobileNo(data.mobileNo)
		})
	}, [])

	return (
	<Fragment>
	<Container>
		
		<h1>Recipient Information</h1>
		
		<CardGroup>
			<Card>
			  <Card.Body>
			    <Card.Text>Full Name: </Card.Text>
			    <Card.Text>Email: </Card.Text>
			    <Card.Text>Address: </Card.Text>
			    <Card.Text>Mobile Number: </Card.Text>
			  </Card.Body>
			</Card>

			<Card>
			  <Card.Body>
			    <Card.Text>{name}</Card.Text>
			    <Card.Text>{email}</Card.Text>
			    <Card.Text>{address}</Card.Text>
			    <Card.Text>{mobileNo}</Card.Text>
			  </Card.Body>
			</Card>
		</CardGroup>	

		<h1>My Cart</h1>
		<ViewCartCard/>
		
	   <Link className= "btn btn-primary" to = {`/`}>Proceed</Link>
	 </Container>
	</Fragment>


	)
}
