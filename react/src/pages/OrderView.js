import {Fragment, useContext} from 'react'
import {Link} from 'react-router-dom'
import {Tabs, Tab} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 
import ViewCartCard from '../components/ViewCartCard'

export default function OrderView() {

	const {user, setUser} = useContext(UserContext)

	const retrieveUserDetails = (token) => {

		fetch('https://deadpoolmagicshop.netlify.app/users/details', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin

			})
		})

	}

	return(
		<Fragment>
			<Tabs defaultActiveKey="currentOrder" id="uncontrolled-tab-example" className="mb-3" tabClassName="text-primary">
			
				<Tab eventKey="currentOrder" title="Orders">
    				
    				<ViewCartCard/>
    				<Link className= "btn btn-primary" to = {`/checkout`}>Proceed</Link>
    				
  				</Tab>

				<Tab eventKey="prev" title="Order History">
				    
				</Tab>
			</Tabs>
		</Fragment>	
	)
}
