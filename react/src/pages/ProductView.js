import { Fragment, useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Card, Container } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext' 
import Swal from 'sweetalert2'

export default function ProductView(){

	const {user} = useContext(UserContext);
	const {productId} = useParams();
	const [product, setProduct] = useState({})
	const history = useNavigate() 


	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice]= useState(0)
	const [stocks, setStocks] = useState(0)
	const [availability, setAvailability] = useState(0)
	const [isOutOfStock, setisOutOfStock] = useState(false)
	const [isActive, setIsActive] = useState(true)
	const [createdOn, setcreatedOn] = useState('')

	


	const purchase = async (productId) => {
		console.log(productId)
		fetch(`https://deadpoolmagicshop.netlify.app/users/purchase`, {
			method: 'POST',
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				userId: user.id
			})
		})
		.then(res =>{
	
			return res.json()
		} )
		.then(data => {
			

			if(data){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product added to cart'
				})

				history("/products")

			} else { 
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}

		})
	}

	useEffect(() => {
		console.log(productId)

		fetch(`https://deadpoolmagicshop.netlify.app/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStocks(data.stocks)
			setAvailability(data.Availability)
			setisOutOfStock(data.isOutOfStock)
			setIsActive(data.isActive)
		

		})
	}, [productId])
	


	return(

		<Container>
			<Row>
				<Col>
					<Card>
						<Card.Body>
						<Container>
						<Row>
							<Col>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description: </Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
					

							{ (user.id !== null && user.isAdmin === false) ?
								<Button variant = "primary" onClick={() => purchase(productId)}>Add to Cart</Button>
								:
								<Button variant = "primary" as={Link} to="/logout" >Login</Button>
							}
							</Col>
						</Row>
						</Container>
						</Card.Body>	
					</Card>	
				</Col>	
			</Row>	
		</Container>	
	)
}
